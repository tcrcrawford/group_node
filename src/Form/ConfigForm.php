<?php

namespace Drupal\group_node\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 *
 * @package Drupal\group_node\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Get the editable config name.
   *
   * @return array
   *   Return array with the editable config name.
   */
  protected function getEditableConfigNames() {
    return [
      'group_node.config',
    ];
  }

  /**
   * Return config form id.
   *
   * @return string
   *   Return config form id as a string.
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * Add fields to the config form. Get content types with the entityManager.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state array.
   *
   * @return array
   *   Returns a config form with the select field of content types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('group_node.config');

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($types as $type) {
      $labels[$type->id()] = $type->id();
    }

    $form['group_content_types'] = [
      '#type' => 'select',
      '#options' => $labels,
      '#title' => $this->t('Content Types'),
      '#multiple' => TRUE,
      '#default_value' => $config->get('group_node.config'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * The submit handler for the configuration form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state array.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('group_node.config')
      ->set('group_node.config', $form_state->getValues())
      ->save();
  }

}
